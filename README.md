# Expense Reimbursement System - Java

## Project Description

The Expense Reimbursement System (ERS) will manage the process of reimbursing employees for expenses incurred while on company time. All employees in the company can login and submit requests for reimbursement and view their past tickets and pending requests. Finance managers can log in and view all reimbursement requests and past history for all employees in the company. Finance managers are authorized to approve and deny requests for expense reimbursement.

## Technologies Used

* Servlets 
* Java 
* JavaScript 
* HTML 
* CSS
* JDBC 
* SQL 
* AJAX 
* Bootstrap 
* RDS 
* Tomcat 
* Git 
* Maven 
* AWS EC2

## Features

List of features ready and TODOs for future development:

* An Employee can login
* An Employee can view the Employee Homepage
* An Employee can logout
* An Employee can submit a reimbursement request
* An Employee can view their pending reimbursement requests
* An Employee can view their resolved reimbursement requests
* An Employee can view their information (profile)

* A Manager can login
* A Manager can view the Manager Homepage
* A Manager can logout
* A Manager can approve/deny pending reimbursement requests
* A Manager can view all pending requests from all employees
* A Manager can view all resolved requests from all employees and see which manager resolved it
* A Manager can view all Employees

To-do list:

* An Employee can upload an image of his/her receipt as part of the reimbursement request
* Password encryption


## Getting Started
   
1) In order to get started clone the repo using:

	`git clone https://gitlab.com/dilig3n7/ers_revproject1.git`

2) Set up the environment variables:(If you are using an EC2, you're going to have to set them on your apache bin folder in the setenv.sh file)

	DB_PASS=(your password)

	DB_URL=jdbc:postgresql://localhost:5432/(YourDataBase)
	
	DB_USER=(your username)

	DB_PASSWORD=(your database password)

3) run the script provided in the project(sqlStatements.sql) on your database to create the necesary
tables. Also create some user Employees and Managers with valid credentials.

4) You will then need to go into the login.js, employee.js, and manager.js files to change the var baseURL to your address.
You can find the files in this path webapp/resources/js

	example) var baseURL="http://localhost:8085/ERS_RevProject1";

	change "http://localhost:8085/ERS_RevProject1" to your base URL

5) Finally, run the project :)


