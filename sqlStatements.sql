create type job_t as enum(
	'Manager',
	'Employee'
)

---<<<<<<<<<<<<<<<<<<<< users table  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>---
create table users (
	user_id serial primary key,
	username varchar(50) unique not null,
	password varchar(50) not null,
	first_name varchar(50) not null,
	last_name varchar(50) not null,
	email varchar(50) unique,
	job_title job_t not null
)

create type item_s as enum(
	'Pending',
	'Approved',
	'Denied'
)

---<<<<<<<<<<<<<<<<<<<< items table  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>---
create table items (
	item_id serial primary key,
	user_id integer references users(user_id) not null,
	posting_date date not null default current_date,
	description varchar(50) unique not null,
	category varchar(50),
	item_cost numeric(7,2) default 0,
	item_status item_s default 'Pending',
	resolved_by integer references users(user_id)
)

-----------------sets the description column on items to ignore cases -------------------------------
CREATE UNIQUE INDEX unique_description_on_items ON items (lower(description))

---<<<<<<<<<<<<<<<<<<<< populate users table  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>---
insert into users (username,password,first_name,last_name,email,job_title) values (
	'dutch123','DPass','Dutch', 'Vandorlin', 'DVandor@rockstar.net', 'Manager'
)
insert into users (username,password,first_name,last_name,email,job_title) values (
	'john123','JPass','John', 'Marston', 'JMarston@rockstar.net', 'Employee'
)
insert into users (username,password,first_name,last_name,email,job_title) values (
	'arthur123','APass','Arthur', 'Morgan', 'AMorgan@rockstar.net', 'Employee'
)
insert into users (username,password,first_name,last_name,email,job_title) values (
	'sadie123','SPass','Sadie', 'Adler', 'SAdler@rockstar.net', 'Employee'
)
insert into users (username,password,first_name,last_name,email,job_title) values (
	'abi123','APass','Abigail', 'Roberts', 'ARoberts@rockstar.net', 'Employee'
)
insert into users (username,password,first_name,last_name,email,job_title) values (
	'mica123','MPass','Micah', 'Bell', 'MBell@rockstar.net', 'Manager'
)
insert into users (username,password,first_name,last_name,email,job_title) values (
	'jack123','JPass','Jack', 'Marston', 'JKMarston@rockstar.net', 'Employee'
);

--drop table items;
--
--delete from items where item_id = 13;
--delete from users where user_id = 9;
--
--insert into items (user_id,description,category,item_cost) values (
--	3,'Rob bank in Saint Denis', null, null
--);
--
--select * from items where user_id = 3 and item_status = 'Pending';
--select * from items where user_id = 3 and item_status = 'Approved' or item_status = 'Denied';
--
--select username,first_name,last_name,email,job_title from users where user_id = 3;
--
--update items set item_status = 'Approved', resolved_by = 5 where item_id = 1;
--update items set item_status = 'Denied', resolved_by = 5 where item_id = 11;
--update items set item_cost = 0 where item_id = 10;
--
--update users set email = 'AMorgan@rockstar.net' where user_id = 2;
--test