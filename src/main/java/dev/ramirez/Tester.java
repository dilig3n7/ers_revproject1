package dev.ramirez;

import dev.ramirez.dao.ItemDAO;
import dev.ramirez.dao.ItemDAOImpl;
import dev.ramirez.dao.UserDAO;
import dev.ramirez.dao.UserDAOImpl;
import dev.ramirez.model.Item;
import dev.ramirez.model.User;
import dev.ramirez.model.status;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.SQLOutput;
import java.util.List;

public class Tester {
    public static void main(String[] args) {
//        int num = 4;
//        String str = String.valueOf(num);
//        System.out.println(str);
        /*-------------------Get all Users Test-------------------*/
//        UserDAO dao = new UserDAOImpl();
//        List<User> users = dao.all_users();
//        for (User user :
//                users) {
//            System.out.println(user.getId() + " : " +user.getUsername() + " : " +
//                    user.getFirstName() + " : " + user.getLastName() +
//                    " : " + user.getEmail() + " : " + user.getJobTitle());
//        }

        /*-------------------All resolved items Test-------------------*/
//        ItemDAO dao = new ItemDAOImpl();
//        List<Item> resolvedItems = dao.all_resolved_items();
//        System.out.println("resolved items");
//        for(Item item: resolvedItems){
//            System.out.println(item.getUser_id() + " : " +item.getPosting_date() + " : " +
//                    item.getDescription() + " : " + item.getCategory() +
//                    " : " + item.getItem_cost() + " : " + item.getItem_status() + " : " + item.getResolved_by());
//        }

        /*-------------------All pending items Test-------------------*/
//        ItemDAO dao = new ItemDAOImpl();
//        List<Item> pendingItems = dao.all_pending_items();
//        System.out.println("pending items");
//        for(Item item: pendingItems){
//            System.out.println(item.getUser_id() + " : " +item.getPosting_date() + " : " +
//                    item.getDescription() + " : " + item.getCategory() +
//                    " : " + item.getItem_cost() + " : " + item.getItem_status());
//        }


//        status mystatus = status.Approved;
//        System.out.println(mystatus.toString());
        /*-------------------resolvedByID Test-------------------*/
//        ItemDAO dao = new ItemDAOImpl();
//        if(dao.resolve_itemByID(12, status.Approved, 5)){
//            System.out.println("success");
//        }
//        else
//            System.out.println("failure");


        /*-------------------View User info Test-------------------*/
//        ItemDAO dao = new ItemDAOImpl();
//        User user = dao.user_infoByID(4);
//        System.out.println(user.getUsername() + " : " + user.getFirstName()
//            + " : "+user.getLastName()+" : "+user.getEmail()+" : "+user.getJobTitle());

        /*-------------------View Pending or Resolved Items Test-------------------*/
//        ItemDAO dao = new ItemDAOImpl();
//        List<Item> pendingItems = dao.pending_itemsByID(3);
//        List<Item> resolvedItems = dao.resolved_itemsByID(3);
//        System.out.println("pending items");
//        for(Item item: pendingItems){
//            System.out.println(item.getPosting_date() + " : " +
//                item.getDescription() + " : " + item.getCategory() +
//                    " : " + item.getItem_cost() + " : " + item.getItem_status());
//        }
//
//        System.out.println();
//        System.out.println("resolved items");
//        for(Item item: resolvedItems){
//            System.out.println(item.getPosting_date() + " : " +
//                    item.getDescription() + " : " + item.getCategory() +
//                    " : " + item.getItem_cost() + " : " + item.getItem_status());
//        }

        /*-------------------Submit Item Test-------------------*/
//        ItemDAO dao = new ItemDAOImpl();
//        Item item = new Item();
//        item.setUser_id(3);
//        item.setDescription("Rob bank in Saint Denis");
//        item.setCategory(null);
//        item.setItem_cost(BigDecimal.valueOf(100.99).setScale(2, RoundingMode.HALF_DOWN));
//
//        if(dao.submit_item(item)){
//            System.out.println("success");
//        }else{
//            System.out.println("failure");
//        }

        /*-------------------User Credential Verification Test-----*/
//        UserDAO dao = new UserDAOImpl();
//        User user = new User();
//        user.setUsername("arthur123");
//        user.setPassword("pass");
//
//        if(dao.verifyCredentials(user)){
//            System.out.println("user is in db");
//        }
//        else
//            System.out.println("user not in db");
    }
}
