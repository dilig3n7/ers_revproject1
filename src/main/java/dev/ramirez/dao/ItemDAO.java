package dev.ramirez.dao;

import dev.ramirez.model.Item;
import dev.ramirez.model.status;

import java.util.List;

public interface ItemDAO {
    public boolean submit_item(Item item);
    public List<Item> pending_itemsByID(int user_id);
    public List<Item> resolved_itemsByID(int user_id);

    //Manager methods
    public boolean resolve_itemByID(int item_id, status item_status, int user_id);
    public List<Item> all_pending_items();
    public List<Item> all_resolved_items();
}
