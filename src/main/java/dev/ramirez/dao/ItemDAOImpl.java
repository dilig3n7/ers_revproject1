package dev.ramirez.dao;

import dev.ramirez.model.Item;
import dev.ramirez.model.User;
import dev.ramirez.model.status;
import dev.ramirez.model.title;
import dev.ramirez.utilities.DAOUtilities;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ItemDAOImpl implements ItemDAO{

    Connection connection = null;
    PreparedStatement stmt = null;

    /*-----------------------------------------------------------*/

    @Override
    public boolean submit_item(Item item) {
        if(item == null){
            return false;
        }
        try{
            BigDecimal item_cost;
            if(item.getItem_cost() == null){
                item_cost = BigDecimal.valueOf(0).setScale(2,RoundingMode.HALF_DOWN);
            }
            else{
                item_cost = item.getItem_cost();
            }
            connection = DAOUtilities.getConnection();
            String sql = "insert into items (user_id,description,category,item_cost) " +
                    "values (?,?,?,?)";
            stmt = connection.prepareStatement(sql);

            stmt.setInt(1, item.getUser_id());
            stmt.setString(2, item.getDescription());
            stmt.setString(3, item.getCategory());
            stmt.setBigDecimal(4, item_cost);

            // If we're able to add our item to the database, we return true.
            // This if statement both executes the query and looks at the return
            // value to determine how many rows were changed
            if(stmt.executeUpdate() != 0){
                return true;
            }
            else
                return false;
        } catch (SQLException e){
            e.printStackTrace();
            return false;
        } finally {
            closeResources();
        }
    }

    /*-----------------------------------------------------------*/

    @Override
    public List<Item> pending_itemsByID(int user_id) {
        if(user_id < 0){
            return null;
        }
        List<Item> items = new ArrayList<Item>();

        try{
            connection = DAOUtilities.getConnection();  // Get our database connection from the manager
            String sql = "select * from items where user_id = '" + user_id + "' and " +
                    "item_status = 'Pending'";
            stmt = connection.prepareStatement(sql);    // Creates the prepared statement from the query

            ResultSet rs = stmt.executeQuery();         // Queries the database

            // So long as the ResultSet actually contains results...
            while (rs.next()){
                // We populate an item object with info for each row from our query result
                Item item = new Item();

                // Each variable in our Task object maps to a column in a row from our results
                item.setPosting_date(rs.getDate("posting_date").toLocalDate());
                item.setDescription(rs.getString("description"));
                item.setCategory(rs.getString("category"));
                item.setItem_cost(rs.getBigDecimal("item_cost").setScale(2, RoundingMode.HALF_DOWN));
                item.setItem_status(status.valueOf(rs.getString("item_status")));

                items.add(item);
            }
            rs.close();
        } catch(SQLException e){
            e.printStackTrace();
            return null;
        } finally {
            closeResources();
        }
        return items;
    }

    /*-----------------------------------------------------------*/

    @Override
    public List<Item> resolved_itemsByID(int user_id) {
        if(user_id < 0){
            return null;
        }
        List<Item> items = new ArrayList<Item>();

        try{
            connection = DAOUtilities.getConnection();  // Get our database connection from the manager
            String sql = "select * from items where user_id = '" + user_id + "' and " +
                    "(item_status = 'Approved' or item_status = 'Denied')";
            stmt = connection.prepareStatement(sql);    // Creates the prepared statement from the query

            ResultSet rs = stmt.executeQuery();         // Queries the database

            // So long as the ResultSet actually contains results...
            while (rs.next()){
                // We populate an item object with info for each row from our query result
                Item item = new Item();

                // Each variable in our item object maps to a column in a row from our results
                item.setPosting_date(rs.getDate("posting_date").toLocalDate());
                item.setDescription(rs.getString("description"));
                item.setCategory(rs.getString("category"));
                item.setItem_cost(rs.getBigDecimal("item_cost").setScale(2, RoundingMode.HALF_DOWN));
                item.setItem_status(status.valueOf(rs.getString("item_status")));

                items.add(item);
            }
            rs.close();
        } catch(SQLException e){
            e.printStackTrace();
            return null;
        } finally {
            closeResources();
        }
        return items;
    }

    /*-----------------------------------------------------------*/


    @Override
    public boolean resolve_itemByID(int item_id, status item_status, int user_id) {
        if(item_id < 0 || user_id < 0 || item_status == null || item_status == status.Pending){
            return false;
        }
        String item_s = item_status.toString();
        try{
            connection = DAOUtilities.getConnection();
            String sql = "update items set item_status = '"+ item_s +"', resolved_by = ? where item_id = ?";
            stmt = connection.prepareStatement(sql);
//            stmt.setString(1, item_status.toString());
            stmt.setInt(1, user_id);
            stmt.setInt(2, item_id);

            // If we're able to resolve the item on the database, we return true.
            // This if statement both executes the query and looks at the return
            // value to determine how many rows were changed
            if(stmt.executeUpdate() != 0){
                return true;
            }
            else{
                return false;
            }
        } catch (SQLException e){
            e.printStackTrace();
            return false;
        } finally {
            closeResources();
        }
    }

    /*-----------------------------------------------------------*/

    @Override
    public List<Item> all_pending_items() {
        List<Item> items = new ArrayList<Item>();

        try{
            connection = DAOUtilities.getConnection();  // Get our database connection from the manager
            String sql = "select * from items where item_status = 'Pending'";
            stmt = connection.prepareStatement(sql);    // Creates the prepared statement from the query

            ResultSet rs = stmt.executeQuery();         // Queries the database

            // So long as the ResultSet actually contains results...
            while (rs.next()){
                // We populate an item object with info for each row from our query result
                Item item = new Item();

                // Each variable in our item object maps to a column in a row from our results
                item.setItem_id(rs.getInt("item_id"));
                item.setUser_id(rs.getInt("user_id"));
                item.setPosting_date(rs.getDate("posting_date").toLocalDate());
                item.setDescription(rs.getString("description"));
                item.setCategory(rs.getString("category"));
                item.setItem_cost(rs.getBigDecimal("item_cost").setScale(2, RoundingMode.HALF_DOWN));
                item.setItem_status(status.valueOf(rs.getString("item_status")));

                items.add(item);
            }
            rs.close();
        } catch(SQLException e){
            e.printStackTrace();
            return null;
        } finally {
            closeResources();
        }
        return items;
    }

    /*-----------------------------------------------------------*/

    @Override
    public List<Item> all_resolved_items() {
        List<Item> items = new ArrayList<Item>();

        try{
            connection = DAOUtilities.getConnection();  // Get our database connection from the manager
            String sql = "select * from items where item_status = 'Approved' or item_status = 'Denied'";
            stmt = connection.prepareStatement(sql);    // Creates the prepared statement from the query

            ResultSet rs = stmt.executeQuery();         // Queries the database

            // So long as the ResultSet actually contains results...
            while (rs.next()){
                // We populate an item object with info for each row from our query result
                Item item = new Item();

                // Each variable in our item object maps to a column in a row from our results
                item.setItem_id(rs.getInt("item_id"));
                item.setUser_id(rs.getInt("user_id"));
                item.setPosting_date(rs.getDate("posting_date").toLocalDate());
                item.setDescription(rs.getString("description"));
                item.setCategory(rs.getString("category"));
                item.setItem_cost(rs.getBigDecimal("item_cost").setScale(2, RoundingMode.HALF_DOWN));
                item.setItem_status(status.valueOf(rs.getString("item_status")));
                item.setResolved_by(rs.getInt("resolved_by"));

                items.add(item);
            }
            rs.close();
        } catch(SQLException e){
            e.printStackTrace();
            return null;
        } finally {
            closeResources();
        }
        return items;
    }

    /*-----------------------------------------------------------*/

    // Closing all resources is important, to prevent memory leaks.
    // Ideally, you really want to close them in the reverse-order you open them
    private void closeResources() {
        try {
            if (stmt != null)
                stmt.close();
        } catch (SQLException e) {
            System.out.println("Could not close statement!");
            e.printStackTrace();
        }

        try {
            if (connection != null)
                connection.close();
        } catch (SQLException e) {
            System.out.println("Could not close connection!");
            e.printStackTrace();
        }
    }
}
