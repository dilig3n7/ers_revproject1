package dev.ramirez.dao;

import dev.ramirez.model.User;

import java.util.List;

public interface UserDAO {
    public boolean verifyCredentials(User user);
    public User getUserByUsername(String username);
    public List<User> all_users();
}
