package dev.ramirez.dao;

import dev.ramirez.model.Item;
import dev.ramirez.model.User;
import dev.ramirez.model.status;
import dev.ramirez.model.title;
import dev.ramirez.utilities.DAOUtilities;

import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

// This class contains the data access methods that will allow to verify user credentials from and
// register new user credential into our database.

public class UserDAOImpl implements UserDAO{

    Connection connection = null;
    PreparedStatement stmt = null;

    /*---------------------------------------------------------------------------------------------*/

    // This method verifies user credencials and returns true or false
    // depending on if user credencials correct and are found in the
    // in the database

    @Override
    public boolean verifyCredentials(User user) {
        User tempUser = new User();
        try{
            connection = DAOUtilities.getConnection();
            String sql = "select * from users where username = '" + user.getUsername()
                    + "' and password = '" + user.getPassword() + "'";
            stmt = connection.prepareStatement(sql);

            ResultSet rs = stmt.executeQuery();

            if(rs.next()){
                tempUser.setUsername(rs.getString("username"));
                tempUser.setPassword(rs.getString("password"));
            }
            if(tempUser.getUsername() == null || tempUser.getPassword() == null){
                return false;
            }
            if(tempUser.getUsername().equals(user.getUsername()) &&
                tempUser.getPassword().equals(user.getPassword())){
                return true;
            }
        } catch(SQLException e){
            e.printStackTrace();
        } finally {
            closeResources();
        }
        return false;
    }

    /*---------------------------------------------------------------------------------------------*/

    @Override
    public List<User> all_users() {
        List<User> users = new ArrayList<User>();

        try{
            connection = DAOUtilities.getConnection();  // Get our database connection from the manager
            String sql = "select * from users";
            stmt = connection.prepareStatement(sql);    // Creates the prepared statement from the query

            ResultSet rs = stmt.executeQuery();         // Queries the database

            // So long as the ResultSet actually contains results...
            while (rs.next()){
                // We populate an item object with info for each row from our query result
                User user = new User();

                // Each variable in our Task object maps to a column in a row from our results
                user.setId(rs.getInt("user_id"));
                user.setUsername(rs.getString("username"));
                user.setFirstName(rs.getString("first_name"));
                user.setLastName(rs.getString("last_name"));
                user.setEmail(rs.getString("email"));
                user.setJobTitle(title.valueOf(rs.getString("job_title")));

                users.add(user);
            }
            rs.close();
        } catch(SQLException e){
            e.printStackTrace();
            return null;
        } finally {
            closeResources();
        }
        return users;
    }

    /*---------------------------------------------------------------------------------------------*/

    @Override
    public User getUserByUsername(String username) {
        if(username == null || username.equals("")){
            return null;
        }
        User user = new User();
        try{
            connection = DAOUtilities.getConnection();  // Get our database connection from the manager
            String sql = "select * from users where username = '" +
                    username+"'";
            stmt = connection.prepareStatement(sql);    // Creates the prepared statement from the query

            ResultSet rs = stmt.executeQuery();         // Queries the database

            // So long as the ResultSet actually contains results...
            if(rs.next()){
                // We populate a User object with info for each row from our query result

                // Each variable in our User object maps to a column in a row from our results
                user.setId(rs.getInt("user_id"));
                user.setUsername(rs.getString("username"));
                user.setFirstName(rs.getString("first_name"));
                user.setLastName(rs.getString("last_name"));
                user.setEmail(rs.getString("email"));
                user.setJobTitle(title.valueOf(rs.getString("job_title")));
            }
            rs.close();
        } catch(SQLException e){
            e.printStackTrace();
            return null;
        } finally {
            closeResources();
        }
        return user;
    }

    /*-----------------------------------------------------------*/

    // Closing all resources is important, to prevent memory leaks.
    // Ideally, you really want to close them in the reverse-order you open them
    private void closeResources() {
        try {
            if (stmt != null)
                stmt.close();
        } catch (SQLException e) {
            System.out.println("Could not close statement!");
            e.printStackTrace();
        }

        try {
            if (connection != null)
                connection.close();
        } catch (SQLException e) {
            System.out.println("Could not close connection!");
            e.printStackTrace();
        }
    }
}
