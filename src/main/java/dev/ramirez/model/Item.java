package dev.ramirez.model;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

public class Item {
    private int item_id = 0;
    private int user_id = 0;
    private LocalDate posting_date = null;
    private String description = null;
    private String category = null;
    private BigDecimal item_cost = null;
    private status item_status = null;
    private int resolved_by = 0;

    public int getItem_id() {
        return item_id;
    }

    public void setItem_id(int item_id) {
        this.item_id = item_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public LocalDate getPosting_date() {
        return posting_date;
    }

    public void setPosting_date(LocalDate posting_date) {
        this.posting_date = posting_date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public BigDecimal getItem_cost() {
        return item_cost;
    }

    public void setItem_cost(BigDecimal item_cost) {
        this.item_cost = item_cost;
    }

    public status getItem_status() {
        return item_status;
    }

    public void setItem_status(status item_status) {
        this.item_status = item_status;
    }

    public int getResolved_by() {
        return resolved_by;
    }

    public void setResolved_by(int resolved_by) {
        this.resolved_by = resolved_by;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Item item = (Item) o;
        return item_id == item.item_id && user_id == item.user_id && resolved_by == item.resolved_by && Objects.equals(posting_date, item.posting_date) && Objects.equals(description, item.description) && Objects.equals(category, item.category) && Objects.equals(item_cost, item.item_cost) && item_status == item.item_status;
    }

    @Override
    public int hashCode() {
        return Objects.hash(item_id, user_id, posting_date, description, category, item_cost, item_status, resolved_by);
    }

    @Override
    public String toString() {
        return "Item{" +
                "item_id=" + item_id +
                ", user_id=" + user_id +
                ", posting_date=" + posting_date +
                ", description='" + description + '\'' +
                ", category='" + category + '\'' +
                ", item_cost=" + item_cost +
                ", item_status=" + item_status +
                ", resolved_by=" + resolved_by +
                '}';
    }
}
