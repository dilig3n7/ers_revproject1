package dev.ramirez.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Objects;

// This class is used as a template for a users credentials
// when registering or logging in

public class User {
    private int user_id = 0;
    private String username = null;
    private String password = null;
    private String firstName = null;
    private String lastName = null;
    private String email = null;
    private title jobTitle = null;

    public int getId() {
        return user_id;
    }

    public void setId(int user_id) {
        this.user_id = user_id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @JsonIgnore
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public title getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(title jobTitle) {
        this.jobTitle = jobTitle;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return user_id == user.user_id && Objects.equals(username, user.username) && Objects.equals(password, user.password) && Objects.equals(firstName, user.firstName) && Objects.equals(lastName, user.lastName) && Objects.equals(email, user.email) && jobTitle == user.jobTitle;
    }

    @Override
    public int hashCode() {
        return Objects.hash(user_id, username, password, firstName, lastName, email, jobTitle);
    }

    @Override
    public String toString() {
        return "User{" +
                "user_id=" + user_id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", jobTitle=" + jobTitle +
                '}';
    }
}
