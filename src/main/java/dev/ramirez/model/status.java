package dev.ramirez.model;

public enum status {
    Pending,
    Approved,
    Denied
}
