package dev.ramirez.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import dev.ramirez.dao.UserDAO;
import dev.ramirez.dao.UserDAOImpl;
import dev.ramirez.model.User;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class getInfoServlet extends HttpServlet {
    private ObjectMapper objectMapper = new ObjectMapper();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String username = req.getParameter("username");

        UserDAO dao = new UserDAOImpl();
        User user = dao.getUserByUsername(username);

        String userJson = objectMapper.writeValueAsString(user);

        resp.setContentType("application/json");
        // Get the printwriter object from response
        // to write the required json object to the output stream
        PrintWriter out = resp.getWriter();
        // returns json object
        out.print(userJson);
        out.flush();
    }
}
