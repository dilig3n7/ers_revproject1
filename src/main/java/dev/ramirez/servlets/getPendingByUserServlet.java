package dev.ramirez.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import dev.ramirez.dao.ItemDAO;
import dev.ramirez.dao.ItemDAOImpl;
import dev.ramirez.dao.UserDAO;
import dev.ramirez.dao.UserDAOImpl;
import dev.ramirez.model.Item;
import dev.ramirez.model.User;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class getPendingByUserServlet extends HttpServlet {
    private ObjectMapper objectMapper = new ObjectMapper();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String userId = req.getParameter("userId");
        int userIdParsed = 0;
        try{
            userIdParsed = Integer.parseInt(userId);
        }catch (Exception e){
            e.printStackTrace();
        }
        ItemDAO dao = new ItemDAOImpl();
        List<Item> items = dao.pending_itemsByID(userIdParsed);

        String itemsJson = objectMapper.writeValueAsString(items);

        resp.setContentType("application/json");
        // Get the printwriter object from response
        // to write the required json object to the output stream
        PrintWriter out = resp.getWriter();
        // returns json object
        out.print(itemsJson);
        out.flush();
    }
}
