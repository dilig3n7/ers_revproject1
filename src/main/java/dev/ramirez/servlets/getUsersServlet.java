package dev.ramirez.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import dev.ramirez.dao.ItemDAO;
import dev.ramirez.dao.ItemDAOImpl;
import dev.ramirez.dao.UserDAO;
import dev.ramirez.dao.UserDAOImpl;
import dev.ramirez.model.Item;
import dev.ramirez.model.User;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class getUsersServlet extends HttpServlet {
    private ObjectMapper objectMapper = new ObjectMapper();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        UserDAO dao = new UserDAOImpl();
        List<User> users = dao.all_users();
        String itemsJson = objectMapper.writeValueAsString(users);

        resp.setContentType("application/json");
        // Get the printwriter object from response
        // to write the required json object to the output stream
        PrintWriter out = resp.getWriter();
        // returns json object
        out.print(itemsJson);
        out.flush();
    }
}
