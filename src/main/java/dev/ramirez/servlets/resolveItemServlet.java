package dev.ramirez.servlets;

import dev.ramirez.dao.ItemDAO;
import dev.ramirez.dao.ItemDAOImpl;
import dev.ramirez.model.Item;
import dev.ramirez.model.status;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;

public class resolveItemServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // get username and password from request
        String userId = req.getParameter("userId");
        String requestId = req.getParameter("request_id");
        String resolve = req.getParameter("resolve");
        if(requestId == ""){
            requestId = "-1";
        }


        int userIdParsed = 0;
        int requestIdParsed = 0;
        try{
            userIdParsed = Integer.parseInt(userId);
            requestIdParsed = Integer.parseInt(requestId);

            ItemDAO dao = new ItemDAOImpl();
            // send 401 if we can't find a user with those credentials
            if(!dao.resolve_itemByID(requestIdParsed, status.valueOf(resolve),userIdParsed)){
                resp.sendError(401, "Item was not resolved.");
            }else{
                resp.setStatus(200);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
