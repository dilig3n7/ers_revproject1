package dev.ramirez.servlets;

import dev.ramirez.dao.ItemDAO;
import dev.ramirez.dao.ItemDAOImpl;
import dev.ramirez.model.Item;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;

public class submitItemServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // get username and password from request
        String userId = req.getParameter("userId");
        String description = req.getParameter("description");
        String category = req.getParameter("category");
        String cost = req.getParameter("cost");
        if(cost == ""){
            cost = "0";
        }

        System.out.println("userId-"+userId+"/desc-"+description+"/cate-"+
                category+"/cost-"+cost);
        int userIdParsed = 0;
        double costParsed = 0;
        try{
            userIdParsed = Integer.parseInt(userId);
            costParsed = Double.parseDouble(cost);

            System.out.println("userIdParsed:"+userIdParsed+"costParsed"+costParsed);

            // check to see if a user/pass match a user in the db
            Item item = new Item();
            item.setUser_id(userIdParsed);
            item.setDescription(description);
            item.setCategory(category);
            item.setItem_cost(BigDecimal.valueOf(costParsed).setScale(2, RoundingMode.HALF_DOWN));

            ItemDAO dao = new ItemDAOImpl();

            // send 401 if we can't find a user with those credentials
            if(description == "" || !dao.submit_item(item)){
                resp.sendError(401, "Item was not submitted.");
            }else{
                resp.setStatus(200);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
