package dev.ramirez.servlets;

import dev.ramirez.dao.UserDAO;
import dev.ramirez.dao.UserDAOImpl;
import dev.ramirez.model.User;
import dev.ramirez.model.title;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class verificationServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // get username and password from request
        String username = req.getParameter("username");
        String pass = req.getParameter("password");

        // check to see if a user/pass match a user in the db
        User user = new User();
        user.setUsername(username);
        user.setPassword(pass);

        UserDAO dao = new UserDAOImpl();

        // send 401 if we can't find a user with those credentials
        if(!dao.verifyCredentials(user)){
            resp.sendError(401, "These credentials aren't valid.");
        }else{
            resp.setStatus(200);
            resp.setContentType("text/html");
            // we also send back and access token that identifies the user
            User verifiedUser = dao.getUserByUsername(user.getUsername());
            String token = verifiedUser.getId()+":"+verifiedUser.getUsername();
            resp.setHeader("Authorization", token);
            resp.setHeader("userID", String.valueOf(verifiedUser.getId()));
            resp.setHeader("title", String.valueOf(verifiedUser.getJobTitle()));
            resp.setHeader("myUsername", "username");

        }
    }
}
