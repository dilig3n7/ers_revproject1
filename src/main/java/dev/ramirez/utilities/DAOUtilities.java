package dev.ramirez.utilities;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

// This class establishes our connection with our database
// We can then use to access our database within the program

public class DAOUtilities {
    private static final String CONNECTION_USERNAME = System.getenv("DB_USER");
    private static final String CONNECTION_PASSWORD = System.getenv("DB_PASS");
    private static final String URL = System.getenv("DB_URL");
    private static Connection connection;

    public static synchronized Connection getConnection() throws SQLException {
        if (connection == null){
            try {
                Class.forName("org.postgresql.Driver");
            } catch (ClassNotFoundException e) {
                System.out.println("Could not register the driver!");
                e.printStackTrace();
            }
            connection = DriverManager.getConnection(URL, CONNECTION_USERNAME, CONNECTION_PASSWORD);
        }

        //if connection was closed then retrieve a new connection
        if (connection.isClosed()) {
//            System.out.println("Opening new connection...");
            connection = DriverManager.getConnection(URL, CONNECTION_USERNAME, CONNECTION_PASSWORD);
        }
        return connection;
    }

}
