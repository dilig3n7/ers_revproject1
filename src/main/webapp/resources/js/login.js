function verifyUser(e){
    e.preventDefault();

    const errorHeader = document.getElementById("errorHeader");
    errorHeader.hidden = true;

    let username = document.querySelector("#uname").value;
    let pass = document.querySelector("#pass").value;

    var data = `username=${username}&password=${pass}`;

var xhr = new XMLHttpRequest();
xhr.withCredentials = true;

xhr.addEventListener("readystatechange", function() {
    if(this.readyState === 4) {
    console.log(this.responseText);
    }
});

xhr.open("POST", "http://3.144.77.207:8080/ERS_RevProject1/login");

xhr.onreadystatechange = function(){
    if(xhr.readyState===4){
        if(xhr.status===401){
            errorHeader.hidden = false;
            errorHeader.innerText = "Invalid Credentials"
        }
        else if(xhr.status ===200){
            const token = xhr.getResponseHeader("Authorization");
            const userID = xhr.getResponseHeader("userID");
            const title = xhr.getResponseHeader("title");
            sessionStorage.setItem("token", token);
            sessionStorage.setItem("userID", userID);
            sessionStorage.setItem("title", title);
            if(title === "Employee"){
                window.location.href="http://3.144.77.207:8080/ERS_RevProject1/employee.html";
            }
            else{
                window.location.href="http://3.144.77.207:8080/ERS_RevProject1/manager.html";
            }
        }else{
            errorHeader.hidden = false;
            errorHeader.innerText = "Unknown Error"
        }
    }
}

xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
xhr.send(data);
}

window.onload = function() {
    sessionStorage.clear();
    let button = document.getElementById("loginBtn");
    button.addEventListener("click", verifyUser);
}
