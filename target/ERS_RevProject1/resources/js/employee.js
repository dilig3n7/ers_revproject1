var firstInfoView = true;
var toggleView = true;
var firstPendingView = true;
var togglePendingView = true;
var firstResolvedView = true;
var toggleResolvedView = true;

function viewInfo(e){
    e.preventDefault();

    if(firstInfoView){
        firstInfoView = false;
        document.getElementById("user-info-table").hidden = false;

        const errorHeader = document.getElementById("errorHeader");
        errorHeader.hidden = true;

        let username = myUsername();

        var xhr = new XMLHttpRequest();
        xhr.withCredentials = true;

        xhr.addEventListener("readystatechange", function() {
            if(this.readyState === 4) {
                console.log(this.responseText);
                const userObj = JSON.parse(this.responseText);

                let tableBody = document.getElementById("info-table");

                let tableRow = document.createElement("tr");

                tableRow.innerHTML = `<td>${userObj.id}</td><td>${userObj.username}</td><td>${userObj.firstName}</td><td>${userObj.lastName}<td>${userObj.email}</td><td>${userObj.jobTitle}</td>`;

                tableBody.appendChild(tableRow);
            }
        });

        xhr.open("GET", `http://3.144.77.207:8080/ERS_RevProject1/info?username=${username}`);

        xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

        xhr.send();
        
    }else if(toggleView){
        document.getElementById("user-info-table").hidden = true;
        toggleView = false;
    }else{
        document.getElementById("user-info-table").hidden = false;
        toggleView = true;
    }
}

window.onload = function () {
    let title = sessionStorage.getItem("title");
    let token = sessionStorage.getItem("token");
    if(title === "Manager" || !token){
        window.location.href="http://3.144.77.207:8080/ERS_RevProject1/login.html";
    }
    document.getElementById("user-info-table").hidden = true;
    let username = myUsername();
    document.getElementById("welcome-h").innerText = `Welcome ${username} :)`;
    let button = document.getElementById("user-info");
    button.addEventListener("click", viewInfo);
    let button1 = document.getElementById("logout-btn");
    button1.addEventListener("click", logOut);
    let button2 = document.getElementById("pending-btn");
    button2.addEventListener("click", getPending);
    let button3 = document.getElementById("resolved-btn");
    button3.addEventListener("click", getResolved);
    let button4 = document.getElementById("submit");
    button4.addEventListener("click", submitRequest);
}

function submitRequest(e){
    e.preventDefault();
    
    const errorHeader = document.getElementById("errorHeader");
    errorHeader.hidden = true;

    let userId = myUserId();
    let desc = document.querySelector("#desc").value;
    let cate = document.querySelector("#cate").value;
    let cost = document.querySelector("#cost").value;

    var data = `userId=${userId}&description=${desc}&category=${cate}&cost=${cost}`;

var xhr = new XMLHttpRequest();
xhr.withCredentials = true;

xhr.addEventListener("readystatechange", function() {
    if(this.readyState === 4) {
    console.log(this.responseText);
    }
});

xhr.open("POST", "http://3.144.77.207:8080/ERS_RevProject1/submit");

xhr.onreadystatechange = function(){
    if(xhr.readyState===4){
        if(xhr.status===401){
            errorHeader.hidden = false;
            errorHeader.innerText = "Invalid Request"
        }
        else if(xhr.status ===200){
            firstPendingView = true;
            var tableRef = document.getElementById("info-pending-table");
            while ( tableRef.rows.length > 0 )
            {
                tableRef.deleteRow(0);
            }
            errorHeader.hidden = false;
            errorHeader.innerText = "Request Submitted!"
            getPending;
        }else{
            errorHeader.hidden = false;
            errorHeader.innerText = "Unknown Error"
        }
    }
}

xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
xhr.send(data);
}

function logOut(e){
    e.preventDefault();
    sessionStorage.clear();
    window.location.href="http://3.144.77.207:8080/ERS_RevProject1/login.html";
}

function getResolved(e){
    e.preventDefault();

    if(firstResolvedView){
        firstResolvedView = false;
        document.getElementById("resolved-table").hidden = false;

        const errorHeader = document.getElementById("errorHeader");
        errorHeader.hidden = true;

        let userId = myUserId();

        var xhr = new XMLHttpRequest();
        xhr.withCredentials = true;

        xhr.addEventListener("readystatechange", function() {
            if(this.readyState === 4) {
            
                let items = JSON.parse(this.responseText);
                let tableBody = document.getElementById("info-resolved-table");

                for(let item of items){
                    let tableRow = document.createElement("tr");

                    tableRow.innerHTML = `<td>${item.posting_date.month}/${item.posting_date.dayOfMonth}/${item.posting_date.year}</td><td>${item.description}</td><td>${item.category}</td><td>$${item.item_cost}</td><td>${item.item_status}</td>`;

                    tableBody.appendChild(tableRow);
                }
            }
        });

        xhr.open("GET", `http://3.144.77.207:8080/ERS_RevProject1/resolved?userId=${userId}`);

        xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

        xhr.send();
        
    }else if(toggleResolvedView){
        document.getElementById("resolved-table").hidden = true;
        toggleResolvedView = false;
    }else{
        document.getElementById("resolved-table").hidden = false;
        toggleResolvedView = true;
    }
}

function getPending(e){
    e.preventDefault();

    if(firstPendingView){
        firstPendingView = false;
        document.getElementById("pending-table").hidden = false;

        const errorHeader = document.getElementById("errorHeader");
        errorHeader.hidden = true;

        let userId = myUserId();

        console.log(userId);

        var xhr = new XMLHttpRequest();
        xhr.withCredentials = true;

        xhr.addEventListener("readystatechange", function() {
            if(this.readyState === 4) {
            
                let items = JSON.parse(this.responseText);
                let tableBody = document.getElementById("info-pending-table");

                for(let item of items){
                    let tableRow = document.createElement("tr");

                    tableRow.innerHTML = `<td>${item.posting_date.month}/${item.posting_date.dayOfMonth}/${item.posting_date.year}</td><td>${item.description}</td><td>${item.category}</td><td>$${item.item_cost}</td><td>${item.item_status}</td>`;

                    tableBody.appendChild(tableRow);
                }
            }
        });

        xhr.open("GET", `http://3.144.77.207:8080/ERS_RevProject1/pending?userId=${userId}`);

        xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

        xhr.send();
        
    }else if(togglePendingView){
        document.getElementById("pending-table").hidden = true;
        togglePendingView = false;
    }else{
        document.getElementById("pending-table").hidden = false;
        togglePendingView = true;
    }
}

function myUsername() {
    let token = sessionStorage.getItem("token");
    let colonIndex = token.indexOf(':');
    let username = token.substring(colonIndex + 1);
    return username;
}

function myUserId(){
    let token = sessionStorage.getItem("token");
    let colonIndex = token.indexOf(':');
    let userId = token.substring(0,colonIndex);
    return userId;
}
